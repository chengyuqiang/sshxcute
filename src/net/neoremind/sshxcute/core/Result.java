package net.neoremind.sshxcute.core;

public class Result {
	
	public int rc;
	
	public String sysout;
	
	public String error_msg;
	
	public boolean isSuccess;
	
	public String jobId;
	
	void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	
	

}
